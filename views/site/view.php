<?php
use yii\helpers\Url;
use yii\helpers\StringHelper;
?>

<div class="row">

    <?php
    if(!Yii::$app->request->get('sub_category')) {
        foreach ($model as $item):

            if(!empty($item['images']['thumb']))
            {
            ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="preview">
                    <a href="<?=Url::to(['site/view','sub_category'=>$item['id']])?>">
                        <img src="/images/<?= $item['alias'] . "/thumb/" . $item['images']['thumb'] ?>" alt="<?= $item['images']['description'] ?>">
                    </a>
                    </div>
                    <div class="caption">
                        <h3><?= $item['title'] ?></h3>
                        <p></p>
                    </div>
                </div>
            </div>
        <?php } endforeach;
    }

    else {
    foreach ($model as $item):?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="preview">
                <a href="/images/<?=$item['subCategory']['alias'] ."/original/". $item['original']?>">
                    <img src="/images/<?=$item['subCategory']['alias'] ."/thumb/". $item['thumb']?>" alt="<?=$item['description']?>">
                </a>
                </div>
                <div class="caption descript">
                    <h3></h3>
                    <p><?=StringHelper::truncate($item['description'],160,'...')?></p>
                    <p><a href="/images/<?=$item['subCategory']['alias'] ."/original/". $item['original']?>" class="btn btn-default" role="button">Оригинальное изображение</a></p>
                </div>
            </div>
        </div>
    <?php endforeach;
    }?>

</div>

