<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изображения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo LinkPager::widget([
        'pagination' => $provider->getPagination(),
    ]);
    ?>
    
   <table class="table table-bordered table-striped">
       <tr>
           <th>ID</th>
           <th>Подкатегория</th>
           <th>Миниатюра</th>
           <th>Описание</th>
           <th>Дата создания/изменения</th>
           <th>Действия</th>
       </tr>

       <?php foreach ($provider->getModels() as $item):?>

           <tr>
               <td><?=$item['id']?></td>
               <td><?=$item['subCategory']['title']?></td>
               <td><img src="/images/<?=$item['subCategory']['alias']."/thumb/".$item['thumb']?>" width="200"></td>
               <td><?=$item['description']?></td>
               <td><?=Yii::$app->formatter->asDate($item['date'])?></td>
               <td>
                   <?=Html::a('Изменить',['update','id'=>$item['id']],['class'=>'btn btn-primary btn-xs'])?>
                   <?=Html::a('Удалить',['delete','id'=>$item['id']],['class'=>'btn btn-danger btn-xs','data'=>['confirm'=>'Удалить изображение?','method'=>'post']])?>
               </td>
           </tr>

       <?php endforeach;?>

   </table>

    <?php
    echo LinkPager::widget([
        'pagination' => $provider->getPagination(),
    ]);
    ?>
</div>
