<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\SubCategory;
use dosamigos\datepicker\DatePicker;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Images */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="images-form">

    <?php $form = ActiveForm::begin(!$model->isNewRecord ? ['enableClientValidation' => false] : ""); ?>

    <?php
        Yii::$app->session->has('sub')&&$model->isNewRecord ? $model->sub_category_id = Yii::$app->session->get('sub') : "";

        $data = \app\models\Category::find()->with('subCategory')->asArray()->all();
        foreach ($data as $items)
        {
            foreach ($items['subCategory'] as $item)
            {
                $sub[$item['id']] = $item['title'];
            }
            $arr[$items['title']] = $sub;
            $sub = '';
        }

        $params = [
            'prompt' => 'Выберите подкатегорию',
        ];
    ?>
    <?= $form->field($model, 'sub_category_id')->dropDownList($arr, $params) ?>

    <?= $form->field($model, 'original')->fileInput() ?>

    <?= $form->field($model, 'description')->widget(Widget::classname(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'pastePlainText' => true,
            'buttonSource' => true,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
            'imageUpload' => Url::to(['/images/image-upload']),
        ]
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
