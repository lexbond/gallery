<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подкатегории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <table class="table table-bordered table-striped">
        <tr>
            <td>ID</td>
            <td>Название</td>
            <td>Категория</td>
            <td>Действия</td>
        </tr>

        <?php foreach ($model as $item): ?>
            <tr>
                <td><?=$item['id']?></td>
                <td><?=$item['title']?></td>
                <td><?=$item['category']['title']?></td>
                <td>
                    <?=Html::a('Изменить',['update', 'id'=>$item['id']],['class'=>'btn btn-primary btn-xs'])?>
                    <?=Html::a('Удалить',['delete', 'id'=>$item['id']],['class'=>'btn btn-danger btn-xs', 'data'=>['confirm'=>'Удалить категорию?','method'=>"post"]])?>

                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>
