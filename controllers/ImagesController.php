<?php

namespace app\controllers;

use app\models\SubCategory;
use app\models\Images;
use app\models\Uploader;
use yii\base\Exception;
use yii\web\UploadedFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ConflictHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
/**
 * ImagesController implements the CRUD actions for Images model.
 */
class ImagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access'=> [
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                        'allow'=>true,
                        'roles'=>['@'],
                        'verbs'=>['POST','GET']
                    ],
                    [
                        'allow'=>false,
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Images models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Images::find()->with('subCategory');

        $provider = new ActiveDataProvider([
            'query'=>$model,
            'pagination'=>[
                'pageSize'=>20,
            ],
        ]);

        return $this->render('index', [
            'model' => $model,
            'provider'=>$provider
        ]);
    }

    /**
     * Displays a single Images model.
     * @param integer $id
     * @return mixed
     */

    /**
     * Creates a new Images model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Images();

        if ($model->load(Yii::$app->request->post())) {

            // Получаем алиас подкатегории для сохранения туда картинок!
            $alias = SubCategory::find()->select('alias')->where(['id'=>$model->sub_category_id])->one();

            $img = new Uploader();

            $img->imageFile = UploadedFile::getInstance($model, 'original');

            // Создаем папки для миниатюры и оригинала
            $img->path = "images/".$alias['alias']."/original/";
            $img->thumbPath = "images/".$alias['alias']."/thumb/";

            $this->createDirectory($img->path);
            $this->createDirectory($img->thumbPath);

            // Заливаем картинки и получаем их имена
            try {
                $images = $img->upload();
            }
            catch (\Exception $e)
            {
                throw new ConflictHttpException(Yii::t('yii', 'Ошибка сохранения. Проверьте права на запись папки /images и вложенных папок.'));
            }



            $model->thumb = $images['thumb'];
            $model->original = $images['original'];
            $model->date = date("Y-m-d H:i:s");


            try {
                $model->save(false);
                Yii::$app->session->set('sub',$model->sub_category_id);
                return $this->redirect(['index']);
            }
            catch (Exception $e)
            {
                throw new ConflictHttpException(Yii::t('yii', 'Ошибка сохранения. Проверьте корректность файла!'));
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Images model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $original = $model->original;

        if ($model->load(Yii::$app->request->post())) {

            // Если файл был заменён, то обрабатываем!

            if($_FILES['Images']['name']['original'])
            {
                // Получаем алиас подкатегории для сохранения туда картинок!
                $alias = SubCategory::find()->select('alias')->where(['id'=>$model->sub_category_id])->one();

                $img = new Uploader();
                $img->imageFile = UploadedFile::getInstance($model, 'original');

                // Создаем папки для миниатюры и оригинала
                $img->path = "images/".$alias['alias']."/original/";
                $img->thumbPath = "images/".$alias['alias']."/thumb/";

                // Заливаем картинки и получаем их имена
                $images = $img->upload();

                $model->thumb = $images['thumb'];
                $model->original = $images['original'];
            }
            else { $model->original = $original; }

            $model->date = date("Y-m-d H:i:s");

            $model->save(false);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Images model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $images = Images::find()->with('subCategory')->where(['id'=>$id])->one();

        // Удаляем запись из базы
        $this->findModel($id)->delete();

        // Удаляем картинки физически
        try
        {
            Images::unlinkImage($images);
        }
        catch (\Exception $e)
        {
            throw new NotFoundHttpException(Yii::t('yii', 'Запись удалена. Картинки не найдены, возможно они были удалены ранее.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Images model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Images the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Images::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function createDirectory($path) {
            try
            {
                FileHelper::createDirectory($path);
            }
            catch (\Exception $e) {
                throw new ConflictHttpException(Yii::t('yii', 'Ошибка создания каталога. Проверьте права на запись директории /web/images '));
            }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/images/upload/', // Directory URL address, where files are stored.
                'path' => '@webroot/images/upload/' // Or absolute path to directory where files are stored.
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/images/upload/', // Directory URL address, where files are stored.
                'path' => '@webroot/images/upload/', // Or absolute path to directory where files are stored.
                'type' => '0',
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/files/', // Or absolute path to directory where files are stored.
                'type' => '1',//GetAction::TYPE_FILES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/files/' // Or absolute path to directory where files are stored.
            ],
        ];
    }
}
