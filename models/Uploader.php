<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.07.17
 * Time: 20:22
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use yii\imagine\Image;


class Uploader extends Model
{
    public $imageFile;
    public $path;
    public $thumbPath;
    public $val = 800;


    public function rules()
    {
        return [
            [['imageFile'], 'file','skipOnEmpty'=>false,'extensions' => 'png, jpg, jpeg','maxSize'=>5242880],
        ];
    }

    public function upload()
    {

        if($this->validate())
        {
            // Определяем новые имена для оригинала и миниатюрки
            $original = time().'.'.$this->imageFile->extension;
            $thumb = "thumb_".$original;

            $this->imageFile->saveAs($this->path.$original);

            // Вычисляем пропорцию и жмём картинку
            $resize = Image::getImagine()->open($this->path.$original);
            $size = $resize->getSize();
            $ratio = $size->getWidth()/$size->getHeight();

            if($size->getWidth() > $size->getHeight())
            {
                $width = $this->val;
                $height = round($width/$ratio);
            }
            else
            {
                $height = $this->val;
                $width = round($height/$ratio);
            }

            $resize->resize(new Box($width,$height))->save($this->thumbPath.$thumb, ['quality'=>80]);

            return ['original'=>$original, 'thumb'=>$thumb];
        }
        else {
            return false;
        }
    }
}