<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property integer $sub_category_id
 * @property string $thumb
 * @property string $original
 * @property string $description
 * @property string $date
 */
class Images extends \yii\db\ActiveRecord
{
    const DEFAULT_PATH = "images";
    const THUMB_PATH = "thumb";
    const ORIGINAL_PATH = "original";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_category_id', 'original', 'description'], 'required'],
            [['sub_category_id'], 'integer'],
            [['original'], 'file','skipOnEmpty'=>false,'extensions' => 'png, jpg, jpeg','maxSize'=>5242880],
            [['description'], 'string'],
            [['date'], 'safe'],
            [['thumb', 'original'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_category_id' => 'Подкатегория',
            'thumb' => 'Миниатюра',
            'original' => 'Изображение',
            'description' => 'Описание',
            'date' => 'Дата создания/Изменения',
        ];
    }

    // Получаем оригинал картинки и её путь


    public function getSubCategory()
    {
        return $this->hasOne(SubCategory::className(), ['id'=>'sub_category_id']);
    }

    public static function unlinkImage($images)
    {
        unlink(Images::DEFAULT_PATH."/".$images['subCategory']['alias']."/".Images::THUMB_PATH."/".$images['thumb']);
        unlink(Images::DEFAULT_PATH."/".$images['subCategory']['alias']."/".Images::ORIGINAL_PATH."/".$images['original']);
    }
}
